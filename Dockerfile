# This Docker image is based on https://github.com/cytopia/docker-ansible

## First Stage: Build Ansible
FROM --platform=$TARGETPLATFORM alpine:3.19 as build

# Install build tools
RUN set -eux \
	&& apk add --update --no-cache \
		bc \
		cargo \
		cmake \
		curl \
		g++ \
		gcc \
		git \
		libffi-dev \
		libxml2-dev \
		libxslt-dev \
		make \
		musl-dev \
		openssl-dev \
		py3-pip \
		python3 \
		python3-dev \
		rust

# Fix: ansible --version: libyaml = True
# https://www.jeffgeerling.com/blog/2021/ansible-might-be-running-slow-if-libyaml-not-available
RUN set -eux \
	&& apk add --update --no-cache \
		py3-yaml \
	&& python3 -c 'import _yaml'

# Install required pip packages
RUN set -eux \
	&& pip3 install --no-cache-dir --no-compile --break-system-packages \
		wheel \
	&& pip3 install --no-cache-dir --no-compile --break-system-packages \
		Jinja2 \
		MarkupSafe \
		PyNaCl \
		bcrypt \
		cffi \
		cryptography \
		pycparser

# Install ansible
RUN pip3 install --no-cache-dir --no-compile --break-system-packages ansible \
    && find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf \
	&& find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf

# Install additional python packages (copied to final image)
RUN set -eux \
	&& apk add --no-cache  \
	&& pip3 install --no-cache-dir --no-compile --break-system-packages \
		junit-xml \
		lxml \
		paramiko \
		ansible-lint \
	&& find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf \
	&& find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf

# Clean-up some site-packages to safe space
RUN set -eux \
	&& pip3 uninstall --yes --break-system-packages \
		setuptools \
		wheel

## Second Stage: Final image
FROM --platform=$TARGETPLATFORM alpine:3.19

# Install packages
RUN set -eux \
	&& apk add --no-cache \
        git \
		git-lfs \
		bash \
		zsh \
		sudo \
		openssh \
		vim \
		nano \
		zsh-autosuggestions \
		zsh-syntax-highlighting \
		bind-tools \
		curl \
# Issue: #85 libgcc required for ansible-vault
		libgcc \
		py3-pip \
		python3 \
# Issue: #76 yaml required for 'libyaml = True' (faster startup time)
		yaml \
	&& ln -sf /usr/bin/python3 /usr/bin/python \
	&& ln -sf ansible /usr/bin/ansible-config \
	&& ln -sf ansible /usr/bin/ansible-console \
	&& ln -sf ansible /usr/bin/ansible-doc \
	&& ln -sf ansible /usr/bin/ansible-galaxy \
	&& ln -sf ansible /usr/bin/ansible-inventory \
	&& ln -sf ansible /usr/bin/ansible-pull \
	&& ln -sf ansible /usr/bin/ansible-test \
	&& find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf \
	&& find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf

COPY --from=build /usr/lib/python3.11/site-packages/ /usr/lib/python3.11/site-packages/
COPY --from=build /usr/bin/ansible /usr/bin/ansible
COPY --from=build /usr/bin/ansible-* /usr/bin/

# Pre-compile Python for better performance
RUN set -eux \
	&& python3 -m compileall /usr/lib/python3.11

# Configure non-root user
ARG USERNAME=ansible
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN addgroup -g $USER_GID $USERNAME \
    && adduser -u $USER_UID -G $USERNAME -s /bin/zsh -D $USERNAME \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

USER ${USERNAME}

# Configure zsh
RUN set -eux \
	&& sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)" \
	&& echo "source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> ~/.zshrc \
	&& echo "source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh" >> ~/.zshrc

CMD ["/bin/bash"]
