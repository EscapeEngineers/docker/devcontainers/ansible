# Ansible Docker Images

This project contains definitions for devcontainer docker images for ansible projects.

## Available Tags

- `latest`

## Available Platforms

All images are available for the following platforms:

* linux/amd64
